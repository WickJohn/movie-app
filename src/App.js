import React, { Component } from 'react';
import Header from './components/Header';
import MovieContainer from './container/MovieContainer';
import DetailPage from './components/page/DetailPage'
import { Route, Switch } from 'react-router-dom';


const App = () => {
  return (
    <div>
      <Header />
      <Switch>
      <Route exact path='/' component={MovieContainer} />
      <Route path='/movie/:id' component={DetailPage} />
      </Switch>
    </div>
  )
}

export default App;
