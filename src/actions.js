import {
  FETCH_MOVIE_REQUEST,
  FETCH_MOVIE_SUCCES,
  SEARCH_MOVIE_SUCCES,
  SEARCH_MOVIE_REQUEST,
  FETCH_DETAIL_REQUEST,
  FETCH_DETAIL_SUCCES} from './actionTypes';

export const fetchMovie = (page) => ({
  type: FETCH_MOVIE_REQUEST,
  page
})

export const fetchMovieSucces = (data) => {
  type: FETCH_MOVIE_SUCCES,
  data
}


export const searchMovie = (query) => ({
  type: SEARCH_MOVIE_REQUEST,
  query
})

export const searchMovieSuccses = (data) => ({
  type: SEARCH_MOVIE_SUCCES,
  data
})


export const fetchDetail = (id) => ({
  type: FETCH_DETAIL_REQUEST,
  id
})

export const fetchDetailSuccses = (data) => ({
  type: FETCH_DETAIL_SUCCES,
  data
}) 
