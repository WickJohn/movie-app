import React, { Component } from 'react';
import {connect} from 'react-redux';
import MovieList from '../components/MovieList';
import {fetchMovie, fetchDetail} from '../actions';




const mapStateToProps = (state)  => ({
  movies : state.movieReducer.movies,
})

function mapDispatchToProps(dispatch) {
  return {
    fetchMovie : (page) => dispatch(fetchMovie(page)),
    fetchDetail: (id) => dispatch(fetchDetail(id)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(MovieList)
