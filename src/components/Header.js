import React from 'react';
import {connect} from 'react-redux';
import {searchMovie} from '../actions';
import { Link } from 'react-router-dom'


class Header extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: ''
    }
  }
  render() {
    const {searchMovie} = this.props
    return (
      <header>
        <Link to="/">
        <div className="header-logo">
          <h1 className="header-title">
          Movie App
        </h1>
      </div>
    </Link>
      <div className="header-form">
        <form onSubmit={(e) => {
          e.preventDefault();
          searchMovie(this.state.value)}}>
          <input type="text" placeholder="Search..."
             onChange={(e) => this.setState({value: e.target.value})} />
             {/* <button type="submit">Search</button> */}
        </form>
      </div>
      </header>
    )
  }
}




function mapDispatchToProps(dispatch) {
  return {
    searchMovie: (query) => dispatch(searchMovie(query))
  }
}

export default connect(null, mapDispatchToProps)(Header);
