import React from 'react';
import {connect} from 'react-redux';
import {fetchDetail} from '../../actions'



class DetailPage  extends React.Component {
  componentDidMount() {
    this.props.fetchDetail(this.props.match.params.id)
  }
render() {
  const {movie, fetchDetail} = this.props
  const baseUrl = "https://image.tmdb.org/t/p/w300"
  const backdropUrl = "https://image.tmdb.org/t/p/original"
  return (

  <div className="movie-detail" style={{backgroundImage: `url(${backdropUrl}${movie.backdrop_path})`}}>
    <div className="movie-detail-overlayer">
    <div className="movie-detail-col1">
    <div className="movie-poster">
      <img src={`${baseUrl}${movie.poster_path}`}/>
    </div>
    <div className="movie-info">
      <h1>{movie.title}</h1>
      <p>Рейтинг: {movie.vote_average}</p>
      <p>Дата: {movie.release_date}</p>
      <p> Страна: {movie.production_countries &&  movie.production_countries.map( c => <span>{c.name}</span>)}</p>
      <p>Жанр: {movie.genres && movie.genres.map(g => <span>{g.name}, </span>)}</p>
      <p></p>
    </div>
  </div>
    <div className="movie-description">
      <h3>Описание</h3>
      <p>{movie.overview}</p>
    </div>
  </div>
  </div>
  )
}
}


const mapStateToProps = (state) => ({
  movie: state.movieDetail,
})

const mapDispatchToProps = dispatch => ({
  fetchDetail: id => dispatch(fetchDetail(id))
})


export default connect(mapStateToProps, mapDispatchToProps)(DetailPage)
