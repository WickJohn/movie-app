import React, { Component } from 'react';
import Movie from './Movie';
import InfiniteScroll from 'react-infinite-scroller';
import { Link } from 'react-router-dom';


 class MovieList extends Component {

   // componentDidMount() {
   //   this.props.fetchMovie()
   // }

  render() {
    const { movies } = this.props
    return (
      <InfiniteScroll
          pageStart={0}
          loadMore={ (page) => this.props.fetchMovie(page) }
          hasMore={true}
          loader={<div className="loader" key={0}>Loading...</div>}
          >
      <div className="list-block">
      {movies.map((movie) => (
        <Link
            
            to={{
              pathname: `/movie/${movie.id}`,
            }} >
        <Movie key={movie.id}
         {...movie}/>
       </Link>
      ))}
      </div>
    </InfiniteScroll>
    )
  }
}


export default MovieList;
