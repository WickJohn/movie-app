import React from 'react';

const Movie = ({ img, poster_path, title, release_date}) => {
  const baseUrl = "https://image.tmdb.org/t/p/w200"
  return (
    <div className="movie-block">
      <img src={`${baseUrl}${poster_path}`} style={{width: '100%'}}/>
      <div className="movie-content">
      <h1 className="movie-title">{title}</h1>
      <p className="movie-ganre">{release_date}</p>
    </div>
  </div>
  )
}

export default Movie;
