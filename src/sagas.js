import { api, apiDetail } from './api';
import { call, put, takeEvery, takeLatest, all } from 'redux-saga/effects'
import {FETCH_MOVIE_REQUEST, SEARCH_MOVIE_REQUEST, FETCH_DETAIL_REQUEST} from './actionTypes';
import { fetchMovieSucces } from './actions';


const API_KEY = "76f2c4c87be8ba0d0796d8698383c4bd"

export function *fetchMovie(action) {
  const response = yield call(api, `https://api.themoviedb.org/3/movie/popular?api_key=${API_KEY}&language=en-US&page=${action.page}&append_to_response=genres`)
  console.log(response)
  yield put({type: 'FETCH_MOVIE_SUCCES', data: response.results})
}

export function *watcherFetchMovie() {
  yield takeEvery(FETCH_MOVIE_REQUEST, fetchMovie)
}


export function *fetchSearchMovie(action) {
  const response = yield call(api, `https://api.themoviedb.org/3/search/movie?api_key=${API_KEY}&language=en-US&query=${action.query}`)
  yield put({type: 'SEARCH_MOVIE_SUCCES', data: response.results})
}

export function *watcherSearchMovie() {
  yield takeLatest(SEARCH_MOVIE_REQUEST, fetchSearchMovie)
}


export function *fetchDetail(action) {
  const response = yield call(apiDetail, `https://api.themoviedb.org/3/movie/${action.id}?api_key=${API_KEY}&language=en-US`)
  console.log('id', action.id)
  yield put({type: 'FETCH_DETAIL_SUCCES', data: response})
}

export function *watcherDetailMovie() {
  yield takeEvery(FETCH_DETAIL_REQUEST, fetchDetail)
}

export function *rootSaga() {
  yield all([
    watcherFetchMovie(),
    watcherSearchMovie(),
    watcherDetailMovie(),
  ])
}
