import { movieReducer } from './movieReducer';
import { movieDetail } from './movieDetailReducer'
import { combineReducers } from 'redux';

const rootReducer = combineReducers({
  movieReducer,
  movieDetail,
})

export default rootReducer;
