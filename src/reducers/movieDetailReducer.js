import {FETCH_DETAIL_SUCCES} from '../actionTypes';



export const movieDetail = (movie={}, action) => {
  switch(action.type) {
    case FETCH_DETAIL_SUCCES :
      return { ...action.data }
    default :
      return movie;
  }
}
