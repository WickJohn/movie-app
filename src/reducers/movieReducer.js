import {FETCH_MOVIE_SUCCES, SEARCH_MOVIE_SUCCES} from '../actionTypes';

const defaultState = {
  currentPage: 0,
  isLoading : false,
  query: '',
  movies: [],
}

export const movieReducer = (state=defaultState, action) => {
  switch(action.type) {
    case FETCH_MOVIE_SUCCES :
      return { ...state, movies: state.movies.concat(action.data)}
    case SEARCH_MOVIE_SUCCES :
      return {...state, movies: action.data}
    default :
      return state;
  }
}
