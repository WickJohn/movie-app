import axios from 'axios'

export const api = (url) => axios.get(url).then(res => res.data)

export const apiDetail = (url) => axios.get(url).then(res => res.data)
